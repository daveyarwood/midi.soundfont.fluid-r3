(set-env!
 :source-paths #{"src"}
 :resource-paths #{"soundfont"}
 :dependencies '[[org.clojure/clojure "1.6.0"]
                 [adzerk/bootlaces "0.1.11" :scope "test"]])

(require '[adzerk.bootlaces :refer :all])

(def +version+ "0.1.1")
(bootlaces! +version+)

(task-options!
  pom {:project 'org.bitbucket.daveyarwood/fluid-r3
       :version +version+
       :description "The freeware General MIDI soundfont FluidR3, (c) Frank Wen"
       :url "https://bitbucket.org/daveyarwood/midi.soundfont.fluid-r3"
       :scm {:url "https://bitbucket.org/daveyarwood/midi.soundfont.fluid-r3"}
       :license {"name" "MIT License"
                 "url" "http://opensource.org/licenses/MIT"}})
