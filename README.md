# midi.soundfont.fluid-r3

A jar containing the excellent freeware General MIDI soundfont [FluidR3](https://github.com/musescore/MuseScore/blob/master/share/sound/FluidR3Mono_License.md).

`midi.soundfont.fluid-r3/sf2` is an input stream handle to the soundfont, for use with [`midi.soundfont`](http://github.com/daveyarwood/midi.soundfont).

FluidR3
© 2000-2002, 2008 Frank Wen <getfrank@gmail.com>
Distributed under the MIT license.
