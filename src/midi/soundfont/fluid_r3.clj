(ns midi.soundfont.fluid-r3
  (:require [clojure.java.io :as io]))

(def sf2
  "An input stream handler for the fluid-r3.sf2 resource."
  (io/input-stream (io/resource "fluid-r3.sf2")))
